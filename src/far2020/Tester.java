package far2020;
import java.util.Scanner;

public class Tester {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		Simulator sim = new Simulator();
		//sim.runLongSimulation();
		int numSteps = 0;
		System.out.println("Number of steps:");
		numSteps = keyboard.nextInt();
		sim.simulate(numSteps);
	}
}

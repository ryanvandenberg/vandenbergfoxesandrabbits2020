package far2020;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.lang.Math;

/**
 * 
 * @author ryanv
 * 
 * An additional actor based off of the monster of the same name from the popular video game "Minecraft".
 * The creature will explode when any other actor comes near it, killing itself and all surrounding creature,
 * and resulting in a fiery crater left behind until it dissipates. The fire left behind by other creepers
 * and the other creepers themselves will also cause the creeper to detonate.
 */
public class Creeper extends Actor{

	// Whether the animal is alive or not.
		private boolean alive;
		// The animals position.
		private Location location;
		
		//the explosion tiles that will be generated if the creeper explodes
		private List<Actor> explosions;
		
		//the chance of a creeper spawning in
		private static final double CREEPER_CREATION_PROBABILITY = 0.01;
		

		
		/**
		 * constructs a new creeper object puts it on the board and makes it alive
		 * @param field the field to add the creeper to
		 * @param location the creeper's location
		 */
		public Creeper(Field field, Location location) {
			super(field);
			setLocation(location);
			alive = true;
			// if(randomAge) {
			/// age = rand.nextInt(MAX_AGE);
			// }
		}
		
		/**
		 * default constructor: creates a blank creeper that doesnt not exist anywhere
		 * @param field
		 */
		public Creeper(Field field) {
			super(field);
			
		}
		
		
		/**
		 * Changes the creepers "living" status true for alive, false for dead
		 * @param alive whether or not the creeper is alive
		 */
		public void setAlive(boolean alive) {
			this.alive = alive;
		}



		 /**
		  * the chance of a creeper being spawned
		  * @return the creeper creation chance
		  */
		 public double getCreeperCreationProbability() {
			 return CREEPER_CREATION_PROBABILITY;
		 }
		 
		 /**
		  * causes the current creeper to die
		  */
			protected void setDead() {
				alive = false;
				if (location != null) {
					super.getField().clear(location);
					location = null;
					super.setField(null);
				}
			}
			

		@Override
		/**
		 * Handles the creepers behavior. A creeper will explode if any other creatures come within an adjacent tile
		 */
		public void act(List<Actor> newActors) {
			explosions = new ArrayList<>();

				if(super.getField().getFreeAdjacentLocations(location).size() < 8) {
					//generates explosion actors in all of the tiles in the specified blast radius
					for(Location local : super.getField().blastRadius(location)) {
						super.getField().clear(local);
						Explosion temp = new Explosion(super.getField(), local);
						super.getField().place(temp, local);
						explosions.add(temp);

					}
					setDead();

				}else {

				}
			
			
		}

		@Override
		/**
		 * tells whether or not the creeper actor is active or not
		 */
		public boolean isActive() {
			return alive;
		}
		
		public List<Actor> getExplosions(){
			return explosions;
		}
		
		/**
		 * Place the animal at the new location in the given field.
		 * 
		 * @param newLocation The animals new location.
		 */
		protected void setLocation(Location newLocation) {
			if (location != null) {
				field.clear(location);
			}
			location = newLocation;
			field.place(this, newLocation);
		}
		
		
		 
		
		//unused code
		//=========================================================
		/**	public void trySpawn() {
		for (int row = 0; row < field.getDepth(); row++) {
			for (int col = 0; col < field.getWidth(); col++) {
				if(field.getObjectAt(row, col) == null) {
					field.place(new Creeper(field,new Location( row, col)), row, col);
				}
			}
		}	
	}
 
 public boolean trySpawn(Creeper creeper, Location location) {
	 if(creeper.isFree(location)) {
		 if(Math.random() < CREEPER_CREATION_PROBABILITY) {
			 return true;
		 }
	 }
	 return false;
 }
 
 public boolean act() {
	 for(int i = location.getRow()-1; i <= location.getRow()+1; i++) {
		 for (int j = location.getCol()-1; j <= location.getCol()+1; j++) {
			 return true;
		 }
	 }
	 return false;
 }


*/
		/**
		 * 
		 * @param location
		 * @return
		 *
		public boolean isFree(Location location)  {
			 
			 if(location == null) {
				 return true;
			 }else {
				 return false;
			 }
		    
		 }
		 */
		 
}

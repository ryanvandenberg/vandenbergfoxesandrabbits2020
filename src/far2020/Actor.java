package far2020;

import java.util.List;

/**
 * 
 * @author ryanv
 *
 *The basic model of objects placed on the field that can interract
 */
public abstract class Actor {
	
	

	protected Field field;

	
	public Actor(Field field) {
		this.field = field;

	}
	
	/**
	 * 
	 * @return field, the actors field
	 */
	public Field getField() {
		return field;
	}

	/**
	 * sets the actors field
	 * @param field the new field
	 */
	public void setField(Field field) {
		this.field = field;
	}



	/**
	 * Performs the action assigned to the specific subclass
	 * @param newActors the new actor objects added that need to act
	 */
	abstract public void act(List<Actor> newActors);
	
	/**
	 * determines if the actor object still needs to be on the field
	 * @return whether or not the object is active
	 */
	abstract public boolean isActive();
	

}

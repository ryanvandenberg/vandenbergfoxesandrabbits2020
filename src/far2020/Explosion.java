package far2020;

import java.util.List;

/**
 * 
 * @author ryanv
 *
 *The explosion class is an actor that is generated when a creeper explodes creating a ring of uninhabitable inferno where the creeper once stood
 */
public class Explosion extends Actor{
	
	private boolean active;
	private int age;
	private Location location;
	
	private static final int MAX_AGE = 3;
	
	
	/**
	 * Creates a new explosion piece
	 * @param field the field to add the exploded tile to
	 * @param location the location of the exploded tile
	 */
	public Explosion(Field field, Location location) {
		super(field);
		age = 0;
		setLocation(location);
		active = true;
	}


	
	@Override
	/**
	 * an exploded tile only ages, sitting there until the land goes back to normal
	 */
	public void act(List<Actor> newActors) {
		//setDead();
		incrementAge();
		// doesnt do anything just exists
		
	}
	
	/**
	 * Increase the age. This could result in the actors death.
	 */
	protected void incrementAge() {
		age++;
		if (age > MAX_AGE) {
			setDead();
		}
	}

	@Override
	/**
	 * tells you if the exploded tile is currently active
	 * @return active, whether or not the tile is active
	 */
	public boolean isActive() {
		return active;
	}
	
	/**
	 * activates the current tile
	 */
	public void activate() {
		active = true;
	}

	/**
	 * removes the exploded tile from the field and clears its location
	 */
	protected void setDead() {
		active = false;
		if (location != null) {
			super.getField().clear(location);
			location = null;
			super.setField(null);
		}
	}
	
	/**
	 * Place the animal at the new location in the given field.
	 * 
	 * @param newLocation The animals new location.
	 */
	protected void setLocation(Location newLocation) {
		if (location != null) {
			field.clear(location);
		}
		location = newLocation;
		field.place(this, newLocation);
	}
}

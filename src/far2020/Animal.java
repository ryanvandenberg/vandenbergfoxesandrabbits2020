package far2020;

import java.util.List;
import java.util.Random;

public abstract class Animal extends Actor{

	// The animals age.

	private int age;

	// Whether the animal is alive or not.
	private boolean alive;
	// The animals position.
	private Location location;


	// A shared random number generator to control breeding.
	private static final Random rand = Randomizer.getRandom();

	/**
	 * Create a new animal
	 * @param field the field to add the animal to 
	 * @param location where in the field the animal is located
	 */
	public Animal(Field field, Location location) {
		super(field);
		setLocation(location);
		
		age = 0;
		alive = true;
		// if(randomAge) {
		/// age = rand.nextInt(MAX_AGE);
		// }
	}

	/**
	 * sets the age of an animal
	 * @param newAge the animals new age
	 */
	public void setAge(int newAge) {
		age = newAge;
	}

	/**
	 * gets the age of the animal
	 * @return the animals current age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * This is what the animals do most of the time. In the process, it might breed,
	 * die of hunger, or die of old age.
	 * 
	 * @param field      The field currently occupied.
	 * @param newAnimals A list to return newly born animals.
	 */
	abstract public void act(List<Actor> newActors);
	
	/**
	 * Place the animal at the new location in the given field.
	 * 
	 * @param newLocation The animals new location.
	 */
	protected void setLocation(Location newLocation) {
		if (location != null) {
			field.clear(location);
		}
		location = newLocation;
		field.place(this, newLocation);
	}
	
	/**
	 * returns the animals current location
	 * @return location, the current location of the animal
	 */
	public Location getLocation() {
		return location;
	}


	/**
	 * Check whether the fox is alive or not.
	 * 
	 * @return True if the fox is still alive.
	 */
	public boolean isAlive() {
		return alive;
	}

	
	/**
	 * Method called when an animal dies
	 */
	protected void setDead() {
		alive = false;
		if (location != null) {
			field.clear(location);
			location = null;
			field = null;
		}
	}

	/**
	 * The breeding status of the animal
	 * @return wether or not the animal is old enough to breed
	 */
	public boolean canBreed() {
		return age >= getBreedingAge();
	}

	/**
	 * Increase the age. This could result in the animals death.
	 */
	protected void incrementAge() {
		age++;
		if (age > getMaxAge()) {
			setDead();
		}
	}

	/**
	 * Generate a number representing the number of births, if it can breed.
	 * 
	 * @return The number of births (may be zero).
	 */
	protected int breed() {
		int births = 0;
		if (canBreed() && rand.nextDouble() <= getBreedingProbability()) {
			births = rand.nextInt(getMaxLitterSize()) + 1;
		}
		return births;
	}

	/**
	 * Returns the age required for an animal to breed
	 * @return breeding age the minimum age to breed
	 */
	abstract protected int getBreedingAge();

	/**
	 * Finds the max age an animal can attain before death
	 * @return age the max age before an animal dies 
	 */
	abstract protected int getMaxAge();

	/**
	 * Returns the animals probability of breeding each turn
	 * @return breeding probability the chance of that animal breeding on a given turn
	 */
	abstract protected double getBreedingProbability();

	/**
	 * Returns the most number of animals that can be created from one breed call
	 * @return max litter size, the most animals created from one breed call
	 */
	abstract protected int getMaxLitterSize();
	
	public boolean isActive() {
		return isAlive();
	}

}
